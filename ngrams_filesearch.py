import gzip
import os

import unidecode

postags = ['NOUN', 'ADJ', 'VERB', 'X', '_']

ngrams_folder = 'ngrams'


def get(query):
	"""Extract from the Goggle Ngrams database all the instances of bigrams in which the given query appears.

	Since no Google Ngrams API is available, a massive download and data crunching of the free data available from their
	website was necessary. The operation reduced tons of GB of data to 17 GB of .txt files and with a further compression
	step the final "database" is just 2.2 GB in size, with search time comparable to an API request from a REST service.

	:param query: the word to be searched in Google Ngrams database
	:return: a tuple containing in the form of (LEMMA, TAG, LEMMA, TAG, FREQ, REVERSED) in which one of the two lemmas
	is the keyword, the FREQ is the frequency of appearance of the bigram in the entire Google Ngrams database when
	searching for the given query and REVERSED is False when the first lemma is the query, True when the second is instead.
	"""
	query = unidecode.unidecode(query).lower()  # uniforms special chars and Title Case

	bigrams = []

	ngrams_name = 'ngrams-{}.txt.gz'.format(query[:3])
	ngrams_path = os.path.join(ngrams_folder, ngrams_name)
	if os.path.exists(ngrams_path):
		ngrams_file = gzip.open(ngrams_path, 'rt')

		line = ngrams_file.readline().strip('\n')
		while line != '':
			line = [e for e in line.split(' ') if e != '']
			if line[0] == query:
				while line[0] == query:
					if line[1] != '_' and line[3] != '_':
						line[4] = int(line[4])
						line.append(False)
						bigrams.append(tuple(line))
					line = ngrams_file.readline().strip('\n')
					line = [e for e in line.split(' ') if e != '']
					if len(line) != 5:
						break
				break
			line = ngrams_file.readline().strip('\n')
		ngrams_file.close()

	ngrams_rev_name = 'ngrams-rev-{}.txt.gz'.format(query[:3])
	ngrams_rev_path = os.path.join(ngrams_folder, ngrams_rev_name)
	if os.path.exists(ngrams_rev_path):
		ngrams_rev_file = gzip.open(ngrams_rev_path, 'rt')

		line = ngrams_rev_file.readline().strip('\n')
		while line != '':
			line = [e for e in line.split(' ') if e != '']
			if line[0] == query:
				while line[0] == query:
					if line[1] != '_' and line[3] != '_':
						line[4] = int(line[4])
						bigrams.append((line[2], line[3], line[0], line[1], line[4], True))
					line = ngrams_rev_file.readline().strip('\n')
					line = [e for e in line.split(' ') if e != '']
					if len(line) != 5:
						break
				break
			line = ngrams_rev_file.readline().strip('\n')
		ngrams_rev_file.close()

	return sorted(bigrams, key=lambda e: e[4], reverse=True)


if __name__ == '__main__':
	keyword = input('Keyword to search: ')
	bigrams = get(keyword)
	print(bigrams)
