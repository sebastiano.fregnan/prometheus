import os
import sys
from enum import Enum


def perrln(msg='', *args):
	print(msg.format(*args), file=sys.stderr, end='\n', flush=True)


def perr(msg='', *args):
	print(msg.format(*args), file=sys.stderr, end='', flush=True)


def assure_dir(*args):
	path = os.path.join(*args)
	if not os.path.exists(path):
		os.mkdir(path)
	return path


class Log(object):
	class Progress(Enum):
		BEGIN = 'begin'
		DOWNLOADING = 'downloading'
		DOWNLOADED = 'downloaded'
		CACHED = 'cached'
		DECOMPRESSING = 'decompressing'
		DECOMPRESSED = 'decompressed'
		DELETING = 'deleting'
		DELETED = 'deleted'
		DONE = 'done'
		LINE = 'line'
		READY = 'ready'
		INSERTING = 'inserting'
		DIVIDING = 'dividing'
	
	_log_file = None
	_log_path = None
	file_code = None
	file_status = None
	file_data = None
	_is_empty = None
	
	def __init__(self, path, truncate=False, resume=True, default_status=Progress.DONE):
		self._log_path = path
		if truncate:
			self._log_file = open(self._log_path, 'w')
			self.file_status = default_status
			self._is_empty = True
		elif os.path.exists(self._log_path):
			if resume:
				log = open(self._log_path, 'r')
				
				# read last line in log file
				log_line = None
				for log_line in log:
					pass
				log.close()
				
				if log_line:
					(self.file_code, log_line) = log_line.strip('\n').split('@', 1)
					(self.file_status, *self.file_data) = log_line.split('-', 1)
					self.file_status = Log.Progress(self.file_status)
					self._is_empty = False
				else:
					self.file_status = default_status
					self._is_empty = True
			else:
				self.file_status = default_status
			# open log in append mode
			self._log_file = open(self._log_path, 'a')
		else:
			# init new log file
			self._log_file = open(self._log_path, 'w')
			self.file_status = default_status
			self._is_empty = True
	
	def __call__(self, status: Progress, data=None, *args, **kwargs):
		self.file_status = status
		if data:
			if isinstance(data, list):
				data = '-'.join(data)
			self._log_file.write('{}@{}-{}\n'.format(self.file_code, self.file_status.value, data))
		else:
			self._log_file.write('{}@{}\n'.format(self.file_code, self.file_status.value))
		self._log_file.flush()
		self._is_empty = False
	
	def file(self, file_code):
		self.file_code = file_code
	
	def trunc(self):
		self.close()
		self._log_file = open(self._log_path, 'w')
		self._is_empty = True
	
	def is_empty(self):
		return self._is_empty
	
	def has_data(self):
		return self.file_data is not None
	
	def close(self):
		self._log_file.close()
