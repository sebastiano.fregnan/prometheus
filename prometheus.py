# Author: Sebastiano Fregnan <sebastiano.fregnan@studenti.univr.it>.

"""An automatic semantically-driven dataset creator of training sets for objects recognition.

This is an implementation of the paper found at https://doi.org/10.1016/j.cviu.2014.07.005 from authors (in alphabetical
 order) Dong Seon Cheng, Francesco Setti, Nicola Zeni, Roberta Ferrario and Marco Cristani.
"""
import hashlib
import shutil
from http.client import RemoteDisconnected
from ssl import CertificateError
from urllib.error import URLError

from requests.exceptions import SSLError, RequestException, Timeout, TooManyRedirects, HTTPError

__version__ = '1.0'
__all__ = [
	"steal"
]

import argparse
import math
import os
import sys
from enum import Enum
from typing import Tuple, List, Union

from urllib.parse import urlsplit
import imagehash

import ngrams_filesearch


def perrln(msg='', *args):
	print(msg.format(*args), file=sys.stderr, end='\n', flush=True)


def perr(msg='', *args):
	print(msg.format(*args), file=sys.stderr, end='', flush=True)


def assure_dir(*args):
	path = os.path.join(*args)
	if not os.path.exists(path):
		os.mkdir(path)
	return path


# check for Python requirements
try:
	import requests
except ImportError:
	perrln("FATAL: missing required \'requests\' module, quitting ...")
	sys.exit(1)
try:
	import unidecode
except ImportError:
	perrln("FATAL: missing required \'unidecode\' module, quitting ...")
	sys.exit(1)
try:
	from google_images_download import google_images_download
except ImportError:
	perrln("FATAL: missing required \'google_images_download\' module, quitting ...")
	sys.exit(1)
try:
	from PIL import Image, ImageChops
except ImportError:
	perrln("FATAL: missing required \'PIL\' module, quitting ...")
	sys.exit(1)
try:
	import nltk
except ImportError:
	perrln("FATAL: missing required \'nltk\' module, quitting ...")
	sys.exit(1)
# check for perceptron tagger
try:
	nltk.data.find('taggers/averaged_perceptron_tagger')
except LookupError:
	perr("WARNING: missing perceptron tagger, downloading it ... ")
	if not nltk.download('averaged_perceptron_tagger', quiet=True):
		perrln("error")
		perrln("FATAL: could not retrieve perceptron tagger, quitting...")
		sys.exit(1)
	perrln("done")
# check for WordNet corpus
try:
	nltk.data.find('corpora/wordnet')
except LookupError:
	perr("WARNING: missing WordNet, downloading it ... ")
	if not nltk.download('wordnet', quiet=True):
		perrln("error")
		perrln("FATAL: could not retrieve WordNet, quitting ...")
		sys.exit(1)
	perr("done")
from nltk.corpus import wordnet as wn


def test():
	steal(
		keyword='aeroplane',
		strategy=Strategy.GOOGLE,
		n_bigrams=10,
		n_imgs=100,
		imgs_size=(32, 32),
		tmp_dir='prometheus_tmp',
		data_dir='prometheus_test_data',
		quiet=False
	)


_quiet = None


class Dataset(object):
	_dest_dir = None
	_title = None
	_dataset_folder = None
	_overall_index = 0

	def __init__(self, dest_dir, title, imgs_size=None):

		self._dest_dir = dest_dir
		self._title = title
		self._imgs_size = imgs_size

		dims = "{}x{}".format(imgs_size[0], imgs_size[1]) if imgs_size is not None else 'any'
		self._dataset_folder = os.path.join(dest_dir, self._title, dims)

	def add(self, src_path, subdirs, strategy, limit):

		# assure each directories exist
		assure_dir(self._dest_dir)
		assure_dir(self._dest_dir, self._title)
		assure_dir(self._dataset_folder)

		strategy_dir = assure_dir(self._dataset_folder, strategy)
		imgs_dir = assure_dir(strategy_dir, 'images')

		# create labels file
		labels_path = os.path.join(strategy_dir, 'labels.txt')
		labels_file = open(labels_path, 'w')

		index = 0
		unique_imgs = set()
		for subdir in subdirs:
			subdir_path = os.path.join(src_path, subdir)
			subdir = subdir.replace(' ', '_')
			for img_name in os.listdir(subdir_path)[:limit]:
				img_src_path = os.path.join(subdir_path, img_name)
				with Image.open(img_src_path) as img:
					hash0 = imagehash.average_hash(img)
					if hash0 not in unique_imgs:
						unique_imgs.add(hash0)
						# resize
						if self._imgs_size:
							bg = Image.new(img.mode, img.size, img.getpixel((0, 0)))
							diff = ImageChops.difference(img, bg)
							diff = ImageChops.add(diff, diff, 2.0, -100)
							bbox = diff.getbbox()
							img = img.resize(self._imgs_size, Image.BICUBIC, bbox)
						# convert to .jpg
						img = img.convert('RGB')
						# save to dataset
						img_label = '{}_{}_{}.jpg'.format(strategy, self._title, index)
						img_dest_path = os.path.join(imgs_dir, img_label)
						img.save(img_dest_path)
						labels_file.write('{} {}\n'.format(img_label[:-4], subdir))
						index += 1
		labels_file.close()
		return self._overall_index

	def get(self):
		return self._dataset_folder


class StrategyManager(object):
	_keyword = None
	_keyword_synset = None
	_keyword_hypos = None
	_keyword_lemmas = None

	_visual_property_synset = None
	_bodily_property_synset = None

	_ngrams_data = []
	_tokens_freq = {}
	_bigrams = []
	_tagged_bigrams = []
	_hypo_bigrams = []
	_vadj_bigrams = []
	_verb_bigrams = []
	_fcomb_bigrams = []
	_ccomb_bigrams = []

	class Strategy(Enum):
		ALL = 'all'
		BASIC = 'basic'
		HYP = 'hyp'
		VADJ = 'vadj'
		PREPAR = 'prepar'
		FCOMB = 'fcomb'
		GOOGLE = 'google'
		VOC = 'voc'

	def __init__(self, keyword, hypernym=None):
		self._keyword = keyword

		####
		# filter using WordNet via NLTK
		#
		# find specific meaning of the keyword by using the provided hypernym when possible
		if not _quiet:
			print('Finding required sense for {} ... '.format(keyword), end='')
		self._keyword_synset = None
		key_synsets = wn.synsets(keyword)
		if hypernym:
			hyper_synsets = wn.synsets(hypernym)
			for key_syn in key_synsets:
				for hyper_syn in hyper_synsets:
					# check if given hypernym is actually an hypernym for the given keyword ...
					common_syn = key_syn.lowest_common_hypernyms(hyper_syn)
					if len(common_syn) != 0 and hyper_syn in common_syn:
						# ... and if so, set the correct meaning of the keyword
						self._keyword_synset = key_syn
						del common_syn, hyper_syn
						break
				if self._keyword_synset:
					del key_syn
					break
			del hyper_synsets
			if not self._keyword_synset and not _quiet:
				print('\n\tERROR: unable to find \'{}\' within the hypernyms of \'{}\''.format(hypernym, keyword))
		if not self._keyword_synset and not _quiet:
			print('\n\tWARNING: no valid hypernym provided, using most frequent sense of \'{}\''.format(keyword))
		if len(key_synsets) != 0:
			# set synset if at least one was found
			self._keyword_synset = key_synsets[0]
		del key_synsets
		if not _quiet:
			print('done')
		
		if self._keyword_synset is not None:
			# recursively extract hypos and lemmas from keyword synset
			self._keyword_hypos = self._keyword_synset.hyponyms()
			self._keyword_lemmas = []
			for hypo in self._keyword_hypos:
				self._keyword_hypos.extend(hypo.hyponyms())
				self._keyword_lemmas.extend([l.lower().split('_') for l in hypo.lemma_names()])
		else:
			self._keyword_hypos = []
			self._keyword_lemmas = []
		# filter nouns, visual adjectives and present participle verbs
		self._visual_property_synset = wn.synsets('visual_property')[0]
		self._bodily_property_synset = wn.synsets('bodily_property')[0]

		if not _quiet:
			print('Searching bigrams for {} ... '.format(keyword), end='')
		self._ngrams_data = ngrams_filesearch.get(self._keyword)
		if not _quiet:
			print('done')

	#
	#   JJ    Adjective
	#   JJS   Adjective, superlative
	#   NN    Noun, singular or mass
	#   NNS   Noun, plural
	#   NNP   Proper noun, singular
	#   NNPS  Proper noun, plural
	#   VBG   Verb, gerund or present participle
	#   VBN   Verb, past participle
	#
	# https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
	#
	def _tag_bigrams(self):
		# tag each bygram (more references at https://www.nltk.org/book/ch05.html)
		if not self._tagged_bigrams:
			if not _quiet:
				print('Tagging bigrams ... ', end='')
			self._tagged_bigrams = nltk.pos_tag_sents([e[0].split(' ') for e in self._bigrams])
			if not _quiet:
				print('done')

	def strategy_basic(self, limit=None):
		# bigrams with nouns, adjectives and verbs
		if self._bigrams:
			return self._bigrams if not limit else self._bigrams[:limit]

		if not _quiet:
			print('Running basic strategy ... ')

		for bigram in self._ngrams_data:
			token = bigram[0] if bigram[5] else bigram[2]
			if (bigram[1], bigram[3]) in [('NOUN', 'NOUN'), ('ADJ', 'NOUN'), ('VERB', 'NOUN')]:
				if token not in self._tokens_freq:
					self._bigrams.append(("{} {}".format(bigram[0], bigram[2]), bigram[4]))
					self._tokens_freq[token] = bigram[4]
				else:
					self._tokens_freq[token] = self._tokens_freq[token] + bigram[4]
		self._bigrams.sort(key=lambda e: e[1], reverse=True)
		if not _quiet:
			print('done')
		return self._bigrams if not limit else self._bigrams[:limit]

	def strategy_hyp(self, limit):
		# from bigrams with nouns, select the ones containing hyponyms
		if len(self._hypo_bigrams) >= limit:
			return self._hypo_bigrams[:limit]

		if not _quiet:
			print('Running hyp strategy ... ')

		if len(self._bigrams) < limit / 5:
			self._bigrams = []
		self.strategy_basic()
		self._tag_bigrams()
		self._hypo_bigrams = []

		for tbi in self._tagged_bigrams:
			# determine the token (namely, the other lemma)
			if tbi[0][0] == self._keyword:
				token_txt = tbi[1][0]
				token_keyword_txt = self._keyword + '_' + token_txt
				token_tag = tbi[1][1]
			else:
				token_txt = tbi[0][0]
				token_keyword_txt = token_txt + '_' + self._keyword
				token_tag = tbi[0][1]

			# extract hyponyms
			if limit > 0:
				# check noun
				if token_tag in ['NN', 'NNS', 'NNP', 'NNPS']:
					token_synsets = wn.synsets(token_txt)
					token_keyword_synsets = wn.synsets(token_keyword_txt)
					# check hyponym
					if any((key_hypo in token_synsets) or (key_hypo in token_keyword_synsets)
					       for key_hypo in self._keyword_hypos) \
							or any(token_txt in lemma for lemma in self._keyword_lemmas):
						if tbi[0][0] == self._keyword:
							self._hypo_bigrams.append(
								('{} {}'.format(self._keyword, token_txt), self._tokens_freq[token_txt]))
						else:
							self._hypo_bigrams.append(
								('{} {}'.format(token_txt, self._keyword), self._tokens_freq[token_txt]))
						limit -= 1
						continue

		if limit != 0 and not _quiet:
			print('hyp strategy: WARNING: only {} hyponyms found'.format(len(self._hypo_bigrams)))

		self._hypo_bigrams.sort(key=lambda e: e[1], reverse=True)
		if not _quiet:
			print('done')
		return self._hypo_bigrams

	def strategy_vadj(self, limit):
		# from bigrams with adjectives, select the ones containing visual or bodily properties
		if len(self._vadj_bigrams) >= limit:
			return self._vadj_bigrams[:limit]

		if not _quiet:
			print('Running vadj strategy ... ')

		if len(self._bigrams) < limit / 5:
			self._bigrams = []
		self.strategy_basic()
		self._tag_bigrams()
		self._vadj_bigrams = []

		for tbi in self._tagged_bigrams:
			# determine the token (namely, the other lemma)
			if tbi[0][0] != self._keyword:
				token_txt = tbi[0][0]
				token_tag = tbi[0][1]

				if limit > 0:
					# check adjective
					if token_tag in ['JJ', 'JJS']:
						token_synsets = wn.synsets(token_txt)
						for syn in token_synsets:
							# check visual or bodily property, no worries about multiple hypernyms at this level in the tree
							if self._visual_property_synset in syn.lowest_common_hypernyms(self._visual_property_synset) \
									or self._bodily_property_synset in syn.lowest_common_hypernyms(
								self._bodily_property_synset):
								self._vadj_bigrams.append(
									('{} {}'.format(token_txt, self._keyword), self._tokens_freq[token_txt]))
								limit -= 1
								continue
		if limit != 0 and not _quiet:
			print('vadj strategy: WARNING: only {} adjectives found'.format(len(self._vadj_bigrams)))

		self._vadj_bigrams.sort(key=lambda e: e[1], reverse=True)
		if not _quiet:
			print('done')
		return self._vadj_bigrams

	def strategy_prepar(self, limit):
		# from bigrams with verbs, select the ones containing present participles
		if len(self._verb_bigrams) >= limit:
			return self._verb_bigrams[:limit]

		if not _quiet:
			print('Running prepar strategy ... ')

		if len(self._bigrams) < limit / 5:
			self._bigrams = []
		self.strategy_basic()
		self._tag_bigrams()
		self._verb_bigrams = []

		for tbi in self._tagged_bigrams:
			# determine the token (namely, the other lemma)
			if tbi[0][0] != self._keyword:
				token_txt = tbi[0][0]
				token_tag = tbi[0][1]

				if limit > 0:
					# check verb
					if token_tag in ['VBG', 'VBN']:
						self._verb_bigrams.append(
							('{} {}'.format(token_txt, self._keyword), self._tokens_freq[token_txt]))
						limit -= 1
						continue

		if limit != 0 and not _quiet:
			print('prepar strategy: WARNING: only {} verbs found'.format(len(self._verb_bigrams)))

		self._verb_bigrams.sort(key=lambda e: e[1], reverse=True)
		if not _quiet:
			print('done')
		return self._verb_bigrams

	#
	# --{48455843414C49425552>>>
	#
	def strategy_fcomb(self, limit):
		# combine the three strategies and extract bigrams based on their global frequency
		if len(self._fcomb_bigrams) >= limit:
			return self._fcomb_bigrams[:limit]

		if not _quiet:
			print('Running fcomb strategy ... ')

		self.strategy_hyp(limit)
		self.strategy_vadj(limit)
		self.strategy_prepar(limit)

		self._fcomb_bigrams = []
		self._fcomb_bigrams.extend(self._hypo_bigrams)
		self._fcomb_bigrams.extend(self._vadj_bigrams)
		self._fcomb_bigrams.extend(self._verb_bigrams)
		self._fcomb_bigrams.sort(key=lambda e: e[1], reverse=True)
		if not _quiet:
			print('done')
		return self._fcomb_bigrams[:limit]

	def strategy_ccomb(self, limit):
		if not _quiet:
			print('Running ccomb strategy ... ', end='')
		# simply run the three strategies
		self.strategy_hyp(limit)
		self.strategy_vadj(limit)
		self.strategy_prepar(limit)
		if not _quiet:
			print('done')
		return True


Strategy = StrategyManager.Strategy


def steal(keyword: str, hypernym: str = None, strategy: Union[List[Strategy], Strategy] = Strategy.ALL,
          n_bigrams: int = 10, n_imgs: int = 100, clean: bool = False, imgs_size: Tuple[int, int] = (640, 480),
          tmp_dir: str = 'tmp', data_dir: str = 'data', quiet: bool = False) -> str:
	"""Automatically create a dataset based on a few given information, namely the actual subject and its logical category.

		If no hypernym is specified, the most common sense of keyword will be used.

	:param keyword: subject upon which the dataset will be constructed
	:param hypernym: a specification term, used to filter out unwanted semantic senses of the keyword
	:param strategy: select the type of strategy to use when creating the dataset
	:param n_bigrams: number of bigrams to extract from each strategy
	:param n_imgs: number of images to download, equally distributed upon the number of bigrams
	:param clean: delete any previously created dataset with this parameters
	:param imgs_size: final size of the images in the dataset
	:param tmp_dir: temporary location for downloaded images
	:param data_dir: dataset final location
	:param quiet: suppress output
	:rtype: str
	:return: the path of the newly created dataset
	"""
	global _quiet
	_quiet = quiet

	# skip execution if already created or delete if necessary
	dims = '{}x{}'.format(imgs_size[0], imgs_size[1]) if imgs_size else 'any'
	dataset_path = os.path.join(data_dir, keyword, dims)
	if os.path.exists(dataset_path):
		if clean:
			shutil.rmtree(dataset_path, ignore_errors=True)
		else:
			if not _quiet:
				print('Dataset {} already created for \'{}\', quitting... '.format(dims, keyword))
			return dataset_path

	# print facade
	if not quiet:
		print(r"""
		____________ ________  ___ _____ _____ _   _  _____ _   _ _____
		| ___ \ ___ \  _  |  \/  ||  ___|_   _| | | ||  ___| | | /  ___|
		| |_/ / |_/ / | | | .  . || |__   | | | |_| || |__ | | | \ `--.
		|  __/|    /| | | | |\/| ||  __|  | | |  _  ||  __|| | | |`--. \
		| |   | |\ \\ \_/ / |  | || |___  | | | | | || |___| |_| /\__/ /
		\_|   \_| \_|\___/\_|  |_/\____/  \_/ \_| |_/\____/ \___/\____/

		      a semantically-driven automatic training set creator
		""")

	####
	# extract bigrams using the required strategies
	#
	fire = StrategyManager(keyword, hypernym)
	strategy = [strategy] if type(strategy) == Strategy else [Strategy.ALL] if type(strategy) != list else strategy
	strategies = {}
	for s in strategy:
		if s == Strategy.ALL:
			strategies = {
				"basic": fire.strategy_basic(n_bigrams),
				"hyp": fire.strategy_hyp(n_bigrams),
				"vadj": fire.strategy_vadj(n_bigrams),
				"prepar": fire.strategy_prepar(n_bigrams),
				"fcomb": fire.strategy_fcomb(n_bigrams),
				"google": [(keyword,)]
			}
		elif s == Strategy.BASIC:
			strategies["basic"] = fire.strategy_basic(n_bigrams)
		elif s == Strategy.HYP:
			strategies["hyp"] = fire.strategy_hyp(n_bigrams)
		elif s == Strategy.VADJ:
			strategies["vadj"] = fire.strategy_vadj(n_bigrams)
		elif s == Strategy.PREPAR:
			strategies["prepar"] = fire.strategy_prepar(n_bigrams)
		elif s == Strategy.FCOMB:
			strategies["fcomb"] = fire.strategy_fcomb(n_bigrams)
		elif s == Strategy.GOOGLE:
			strategies["google"] = [(keyword,)]

	####
	# batch processing
	#
	assure_dir(tmp_dir)
	dataset = Dataset(data_dir, keyword, imgs_size)
	for strategy, bigrams in strategies.items():

		if not _quiet:
			print('Processing strategy: {}'.format(strategy))

		if len(bigrams) == 0 and not _quiet:
			print('No bigrams found for this strategy, skipping...')
			continue

		bigrams = [e[0] for e in bigrams]
		imgs_per_bigram = math.floor(n_imgs / len(bigrams))

		# Google search images and download
		if not _quiet:
			print('\tDownloading images ... ')
		res = _cached_google_download(bigrams, tmp_dir, imgs_per_bigram)
		if not _quiet:
			print('\t\t{} completely cached bigrams'.format(len(bigrams) - len(res['cached']) - len(res['downloaded'])))
			print('\t\t{} partially cached bigrams'.format(len(res['cached'])))
			print('\t\t{} downloaded bigrams'.format(len(res['downloaded'])))
			print('\tdone')

		#
		# inevitably, outliers (drawings, unrelated images, collages) will be collected. a huge improvement would be
		# deleting them using cv techniques, but it is not required in this implementation
		#

		# manipulate, rename, label, generate dataset
		if not _quiet:
			print('\tAdding images to dataset ... ', end='')
		dataset.add(tmp_dir, bigrams, strategy, imgs_per_bigram)
		if not _quiet:
			print('done')

	print('Dataset creation completed for keyword: {}'.format(keyword))

	return dataset.get()


def _cached_google_download(queries, cache_folder, limit):
	res = {'cached': {}, 'downloaded': {}}
	# check that each query has at least the minimum amount of images required
	cached_queries = []
	new_queries = []
	cache = os.listdir(cache_folder)
	for query in queries:
		# check the presence of a cache for a given query
		if query in cache:
			n_cached_imgs = len(os.listdir(os.path.join(cache_folder, query)))
			if n_cached_imgs < limit:
				cached_queries.append((query, n_cached_imgs))
		else:
			new_queries.append(query)
	# download missing images if cache is not enough
	if cached_queries:
		for query, n_cached_imgs in cached_queries:
			paths, n_err = _google_download(query, min(limit + n_cached_imgs, 100), cache_folder, offset=n_cached_imgs)
			for k, v in paths.items():
				res['cached'][k] = v
	# download from new queries (if not in cached mode, download immediately everything)
	if new_queries:
		res['downloaded'] = _google_download(','.join(new_queries), limit, cache_folder)[0]
	return res


def _google_download(query, limit, directory, offset=0):
	# redirect unwanted output to null device
	devnull = open(os.devnull, 'w')
	stdout = sys.stdout
	sys.stdout = devnull
	# download images
	response = google_images_download.googleimagesdownload()
	images_urls, errors = response.download({
		'keywords': query,
		'limit': limit,
		'offset': offset,
		'format': 'jpg',
		'no_download': True,
		'silent_mode': True
	})
	# avoid empty set given by bugged library
	query = []
	for q, urls in images_urls.items():
		if len(urls) == 0:
			query.append(q)
	while query:
		query = ','.join(query)
		# obtain missing urls
		more_images_urls, more_errors = response.download({
			'keywords': query,
			'limit': limit,
			'offset': offset,
			'format': 'jpg',
			'no_download': True,
			'silent_mode': True
		})
		query = []
		for more_q, more_urls in more_images_urls.items():
			images_urls[more_q] = more_urls
			# avoid empty set given by bugged library
			if len(more_urls) == 0:
				query.append(more_q)
		errors += more_errors
	# save images
	for q, urls in images_urls.items():
		q_dir = assure_dir(directory, q)
		for url in urls:
			img_name = hashlib.sha224(os.path.split(urlsplit(url).path)[1].encode('utf-8')).hexdigest() + '.jpg'
			img_path = os.path.join(q_dir, img_name)
			try:
				img_raw = requests.get(url, timeout=10).content
				with open(img_path, 'wb') as f:
					try:
						f.write(img_raw)
					except:
						pass
				if os.path.exists(img_path):
					try:
						with Image.open(img_path):
							pass
					except:
						os.remove(img_path)
						errors += 1
				else:
					errors += 1
			except CertificateError as e:
				errors += 1
			except SSLError as e:
				errors += 1
			except RemoteDisconnected as e:
				errors += 1
			except TimeoutError as e:
				errors += 1
			except URLError as e:
				errors += 1
			except RequestException as e:
				errors += 1

	# restore default stdout
	sys.stdout = stdout
	devnull.close()
	return images_urls, errors


def _setup_cli_options_parser():
	parser = argparse.ArgumentParser(prog="prometheus", description='A semantically-driven image training set creator.')
	# positional arguments
	parser.add_argument('query', metavar='keyword',  # dest='query'
	                    help='the subject upon which the set will be created')
	# optional arguments
	parser.add_argument('-hy', '--hypernym', metavar='H', dest='hyper',
	                    help='set the hypernym of the keyword for a better search')

	parser.add_argument("-s", "--strategy", metavar='S', dest='strategy', type=Strategy, nargs='*',
	                    default=Strategy.ALL, choices=[Strategy.ALL, Strategy.BASIC, Strategy.HYP, Strategy.VADJ,
	                                                   Strategy.PREPAR, Strategy.FCOMB, Strategy.GOOGLE],
	                    help='strategy to utilize when constructing the dataset')

	parser.add_argument('-b', '--bigrams', metavar='N', dest='n_bigrams', type=int, default=10,
	                    help='set the maximum number of bigrams to utilize for each semantic category')
	parser.add_argument('-i', '--images', metavar='N', dest='n_imgs', type=int, default=100,
	                    help='set the maximum number of images to be downloaded for each bigram')

	parser.add_argument("-c", "--clean", dest='clean', action="store_true",
	                    help='override the any other existing dataset with these parameters')

	parser.add_argument('-d', '--images-dimensions', metavar=('W', 'H'), dest='imgs_size', type=int, nargs=2,
	                    default=(640, 480),
	                    help='set the dimensions to which the downloaded images will be scaled and eventually cropped')
	parser.add_argument('-tmp', '--temp-folder', metavar='PATH', dest='tmp_path', default='tmp',
	                    help='set the path of the temporary downloaded images')
	parser.add_argument('-data', '--data-folder', metavar='PATH', dest='data_path', default='data',
	                    help='set the path where the datasets will be crated')

	parser.add_argument("-q", "--quiet", dest='quiet', action="store_true",
	                    help='omit Prometheus console outputs')

	return parser.parse_args()


if __name__ == "__main__":
	# test()
	args = _setup_cli_options_parser()
	steal(
		keyword=unidecode.unidecode(args.query).lower(),
		hypernym=args.hyper.lower() if args.hyper else None,
		strategy=args.strategy,
		n_bigrams=args.n_bigrams,
		n_imgs=args.n_imgs,
		clean=args.clean,
		imgs_size=args.imgs_size,
		tmp_dir=args.tmp_path,
		data_dir=args.data_path,
		quiet=args.quiet
	)
