import sqlite3

from utils import *

alpha = 'abcdefghijklmnopqrstuvwxyz'
postags = ["NOUN", "VERB", "ADJ", "ADV", "PRON", "DET", "ADP", "NUM", "CONJ", "PRT", ".", "X"]

query_create_db = """
CREATE TABLE word (lemma VARCHAR UNIQUE NOT NULL);
CREATE UNIQUE INDEX idx_word_lemma ON word(lemma DESC);
CREATE TABLE bigram (
	lemma1 INTEGER NOT NULL REFERENCES word(rowid),
	tag1 VARCHAR CHECK (tag1 IN ('NOUN', 'VERB', 'ADJ', 'ADV', 'PRON', 'DET', 'ADP', 'NUM', 'CONJ', 'PRT', '.', 'X')), 
	lemma2 INTEGER NOT NULL REFERENCES word(rowid),
	tag2 VARCHAR CHECK (tag2 IN ('NOUN', 'VERB', 'ADJ', 'ADV', 'PRON', 'DET', 'ADP', 'NUM', 'CONJ', 'PRT', '.', 'X')),
	frequency INTEGER NOT NULL, 
	PRIMARY KEY (lemma1, tag1, lemma2, tag2)
);
CREATE INDEX idx_bigram_lemma1 ON bigram(lemma1 DESC);
CREATE INDEX idx_bigram_lemma2 ON bigram(lemma2 DESC);
"""
query_select_word = "SELECT rowid FROM word WHERE lemma = ?"
query_select_bigram = "SELECT rowid FROM bigram WHERE lemma1 = ? AND lemma2 = ? AND tag1 = ? AND tag2 = ?"
query_insert_word = "INSERT OR IGNORE INTO word VALUES (?)"
query_insert_bigram = "INSERT OR IGNORE INTO bigram VALUES (?, ?, ?, ?, ?)"


def main(first_letter=None, second_letter=None):
	db_folder = 'db'
	if not os.path.exists(db_folder):
		os.mkdir(db_folder)
	
	db_name = 'ngrams.db'
	db_path = os.path.join(db_folder, db_name)
	
	# check db existence
	if not os.path.exists(db_path):
		conn = sqlite3.connect(db_path)
		conn.executescript(query_create_db)
	else:
		conn = sqlite3.connect(db_path)
	conn.isolation_level = None
	cur = conn.cursor()
	
	# init log
	log_name = 'progress.log'
	log_path = os.path.join(db_folder, log_name)
	
	# ignore log if offset specified
	if not first_letter:
		log = Log(log_path, resume=True)
		# skip already done files
		if log.is_empty():
			a_offset = 0
			b_offset = 0
		else:
			a_offset = alpha.index(log.file_code[0])
			b_offset = alpha.index(log.file_code[1])
			if log.file_status is Log.Progress.DONE:
				b_offset += 1
	else:
		log = Log(log_path, resume=False)
		a_offset = alpha.index(first_letter)
		b_offset = alpha.index(second_letter) if second_letter else 0
	
	file_txt, line, line_idx, done_size = None, [], 0, 0
	
	def _readline():
		nonlocal file_txt, line, line_idx, done_size
		line = file_txt.readline()
		done_size += len(line)
		line = line.strip('\n').replace(' ', '\t').split('\t')
		line_idx += 1
		return len(line) == 3
	
	def _insert_word(idx):
		nonlocal line, cur
		lemma, *tag = line[idx].rsplit('_', 1)
		if not tag or tag[0] not in postags:
			lemma = line[idx]
			tag = None
		else:
			tag = tag[0]
		cur.execute(query_insert_word, (lemma,))
		if cur.rowcount == 1:
			lemma = cur.lastrowid
		else:
			cur.execute(query_select_word, (lemma,))
			lemma = cur.fetchone()[0]
		return lemma, tag
	
	google_ngrams_folder = 'google_ngrams'
	
	for a in alpha[a_offset:]:
		for b in alpha[b_offset:]:
			log_file_name = a + b
			file_txt_name = 'googlebooks-eng-all-2gram-20120701-{}.txt'.format(log_file_name)
			file_txt_path = os.path.join(google_ngrams_folder, file_txt_name)
			file_txt_size = os.path.getsize(file_txt_path)
			
			if log.file_status is Log.Progress.DONE:
				log.file(log_file_name)
				log(Log.Progress.BEGIN)
			
			if log.file_status is Log.Progress.BEGIN \
				or log.file_status is Log.Progress.READY \
				or log.file_status is Log.Progress.INSERTING \
				or log.file_status is Log.Progress.LINE:
				with open(file_txt_path, 'r', encoding='utf-8') as file_txt:
					
					if log.file_status is Log.Progress.BEGIN:
						perrln('Begin processing file {}', file_txt_name)
						log(Log.Progress.READY)
					
					if log.file_status is Log.Progress.LINE and log.has_data():
						perr('Interrupted, restoring previous checkpoint for {} ...', file_txt_name)
						for i in range(int(log.file_data[0])-1):
							# redo last line
							file_txt.readline()
						perrln('done')
						log(Log.Progress.READY)
					
					if log.file_status is Log.Progress.INSERTING:
						perrln('Interrupted, no checkpoint saved for {}', file_txt_name)
						log(Log.Progress.READY)
					
					if log.file_status is Log.Progress.READY:
						perr(' => inserting lemmas and bigrams ... ')
						log(Log.Progress.INSERTING)
						try:
							batch_size = 1000
							batch_idx = 0
							batches_failed = {}
							done = False
							done_size = 0
							perc_label = list(range(5, 101, 5))
							while not done:
								batch_idx += 1
								batch = []
								for i in range(batch_size):
									if not _readline():
										done = True
										break
									else:
										lemma1, tag1 = _insert_word(0)
										lemma2, tag2 = _insert_word(1)
										batch.append((lemma1, tag1, lemma2, tag2, line[2]))
								cur.executemany(query_insert_bigram, batch)
								if cur.rowcount != batch_size:
									# cur.execute(query_select_bigram, (lemma1, lemma2, tag1, tag2))
									# perrln('skipping "{} {}", rowid {}', line[0], line[1], cur.fetchone()[0])
									batches_failed[batch_idx] = batch
									perrln('error in batch {}', batch_idx)
									perr(' => inserting lemmas and bigrams ... ')
								perc = round(100 * done_size / file_txt_size, 1)
								if int(perc) % 5 == 0 and perc_label in perc_label:
									perr('#')
									perc_label.remove(int(perc))
							log(Log.Progress.DONE)
							perrln(' done')
						except BaseException as ex:
							perrln('error {}', ex)
							log(Log.Progress.LINE, line_idx)


if __name__ == '__main__':
	l1 = sys.argv[1] if len(sys.argv) > 1 else None
	l2 = sys.argv[2] if len(sys.argv) > 2 else None
	main(l1, l2)
