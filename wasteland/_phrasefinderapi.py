import urllib

import requests

from utils import *


def lookup(query, nresults, nretries=3, wait=5):
	payload = {
		'corpus': 'eng-us',
		'query': urllib.parse.quote(query),
		# max phrases to return
		'topk': nresults
	}
	payload = '&'.join(['{}={}'.format(k, v) for k, v in payload.items()])

	ntimes = 0
	while ntimes <= nretries:
		response = requests.get(
			'https://api.phrasefinder.io/search?{}'.format(payload))  # TODO can be done in batch, check errors

		if response.status_code == 200:
			# OK
			return response.json()

		elif response.status_code == 400:
			# bad request
			# shouldn't be possible, so this is very bad
			response_json = response.json()
			perrln('ERROR: unable to query \'{}\' to PhraseFinder, error code {} \"{}\"'
			       .format(query, response_json['error']['code'], response_json['error']['message']))
			return None

		elif response.status_code >= 500:
			# retry
			nretries += 1
			perr('ERROR: unable to query \'{}\' to PhraseFinder, status code {}. Retrying in {} seconds... '
			     .format(query, response.status_code, nretries * wait))
			import time
			time.sleep(nretries * wait)
			perrln('retrying')
			continue

		else:
			# weird fucked up shit here
			perrln('ERROR: unexpected response from PhraseFinder, status code {}'.format(response.status_code))
			return None

	perrln('ERROR: query to PhraseFinder failed {} times, task will be discarded'.format(nretries))
	return None
