import os
import random

import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
from joblib import dump, load
from sklearn.metrics import classification_report, confusion_matrix, average_precision_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

import prometheus
from utils import assure_dir

voc_classes = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
               'cow', 'dog', 'horse', 'motorbike', 'person', 'sheep', 'sofa', 'train', 'tv']

voc_classes = voc_classes[:]
n_voc_classes = len(voc_classes)


def main():
	""" KNN metrics

	for each class we build a binary classifier, with N positives and N negatives (where N is dimensionality of the
	PASCAL VOC 2012 training set for each class) and the negatives are randomly taken from the positives of the other
	classes in a uniform way (each class contributes with the same number of samples when creating the negative class)


	FOR FCOMB

	each image is 32x32 and the feature descriptors are extracted simply by considering the RGB values of each pixel,
	top to bottom, right to left

	when selecting neighbors, the metric is the sum of squared distances (SSD); each positive neighbor is a +1, each
	negative is a -1, the sum of them individuates the winning class considering the sign and a sort of "confidence"
	considering the module. K = 49


	FOR CCOMB

	a classifier for each hyp, vadj, prepar is initiated (the negatives are extracted on the same fashion as before)
	"""

	debug = True
	n_imgs = 100
	imgs_size = (32, 32)

	# init a dataset for each class
	data_paths = []
	for clazz in voc_classes:
		keyword_dataset_path = \
			prometheus.steal(clazz, n_bigrams=10, n_imgs=n_imgs, imgs_size=imgs_size, clean=False, quiet=not debug)
		data_paths.append(keyword_dataset_path)

	####
	# prepare for KNN
	#
	datasets_dir = assure_dir('datasets')
	datasets_train_dir = assure_dir(datasets_dir, 'train')
	datasets_test_dir = assure_dir(datasets_dir, 'test')
	dataset_train_voc_dir = assure_dir(datasets_train_dir, 'voc')
	# names of the feature files
	train_prometheus_filename = 'train_prometheus_knn_features.csv'
	train_voc_filename = 'train_voc_knn_features.csv'
	test_filename = 'test_knn_features.csv'
	# create features files for each prometheus strategy of each class and its voc dataset, and for test
	for data_dir in data_paths:

		# data_dir is in the pattern <data>/<keyword>/<HxW>/<strategies>
		data_class = os.path.split(os.path.split(data_dir)[0])[1]
		strategies = os.listdir(data_dir)

		dataset_train_voc_class_dir = assure_dir(dataset_train_voc_dir, data_class)
		dataset_train_voc_class_dir_content = os.listdir(dataset_train_voc_class_dir)

		dataset_test_class_dir = assure_dir(datasets_test_dir, data_class)
		dataset_test_class_dir_content = os.listdir(dataset_test_class_dir)
		
		avg_n_images_in_strategy = 0

		# create train set each Prometheus train dataset
		for strategy in strategies:

			if debug:
				print('Preparing KNN features for \'{}/{}\' ... '.format(data_class, strategy), end='')

			dataset_train_strategy_dir = assure_dir(datasets_train_dir, strategy)
			dataset_train_strategy_class_dir = assure_dir(dataset_train_strategy_dir, data_class)
			dataset_train_strategy_class_dir_content = os.listdir(dataset_train_strategy_class_dir)

			data_strategy_dir = os.path.join(data_dir, strategy)
			data_strategy_images_dir = os.path.join(data_strategy_dir, 'images')
			n_images_in_strategy = len(os.listdir(data_strategy_images_dir))
			avg_n_images_in_strategy += n_images_in_strategy

			# create train set from Prometheus strategy-specific dataset
			if train_prometheus_filename not in dataset_train_strategy_class_dir_content:
				dataset = []
				####
				# get negatives
				#
				# get "equally" distributed map for images extraction from dataset
				distribution = []
				n_images = n_images_in_strategy
				n_other_classes = n_voc_classes - 1
				for i in range(n_other_classes, 0, -1):
					distribution.append(n_images // i)
					n_images -= n_images // i
				# include negative samples in dataset
				other_data_classes = [e for e in voc_classes if e != data_class]
				i = 0
				while i < len(other_data_classes):
					if distribution[i] == 0:
						i += 1
						continue
					other_data_class = other_data_classes[i]
					other_data_dir = [e for e in data_paths if other_data_class in e][0]
					other_data_strategy_images_dir = os.path.join(other_data_dir, strategy, 'images')
					# a data dir may not have the specified strategy
					if not os.path.exists(other_data_strategy_images_dir):
						# if not last class to be added
						if i != n_other_classes - 1:
							if distribution[i + 1] == 0:
								# back coming from the next class, keep going back
								distribution[i - 1] = distribution[i]
								distribution[i] = 0
								i -= 1
							else:
								# increment contribution of next class and skip this one
								distribution[i + 1] += distribution[i]
								i += 1
							continue
						else:
							# increment contribution of previous class and step back
							distribution[i - 1] = distribution[i]
							distribution[i] = 0
							i -= 1
							continue

					other_data_strategy_images_dir_content = os.listdir(other_data_strategy_images_dir)
					n_other_data_strategy_images_dir_content = len(other_data_strategy_images_dir_content)
					# if this data dir does not have enough samples, include from others
					if n_other_data_strategy_images_dir_content < distribution[i]:
						# if not last class to be added
						if i != n_other_classes - 1:
							# increment contribution of next class by the missing samples count in this one
							distribution[i + 1] += (distribution[i] - n_other_data_strategy_images_dir_content)
							distribution[i] = n_other_data_strategy_images_dir_content
						else:
							# reset contribution of first available (may be unable to contibute if folder is empty)
							# previous class by the missing samples count in this one (since it has already contributed
							# with its samples) and step back
							this_i = i
							while distribution[i - 1] == 0:
								i -= 1
							distribution[i - 1] = (distribution[this_i] - n_other_data_strategy_images_dir_content)
							distribution[this_i] = n_other_data_strategy_images_dir_content
							i -= 1
							# get the previous available class and work from there
							other_data_dir = [e for e in data_paths if other_data_classes[i] in e][0]
							other_data_strategy_images_dir = os.path.join(other_data_dir, strategy, 'images')

					# contribute with negative samples taken from this data dir
					for img in random.sample(other_data_strategy_images_dir_content, distribution[i]):
						dataset.append((os.path.join(other_data_strategy_images_dir, img), 0))
					i += 1

				####
				# get positives
				#
				data_strategy_images_dir_content = os.listdir(data_strategy_images_dir)
				for img in data_strategy_images_dir_content:
					dataset.append((os.path.join(data_strategy_images_dir, img), 1))

				# add a little bit of entropy
				random.shuffle(dataset)

				####
				# create features file
				#
				knn_features_path = os.path.join(dataset_train_strategy_class_dir, train_prometheus_filename)
				knn_features_file = open(knn_features_path, 'w')

				# write title in csv
				title = ['r_{n};g_{n};b_{n}'.format(n=e) for e in range(imgs_size[0] * imgs_size[1])]
				title.append('class')
				title = ';'.join(title)
				knn_features_file.write(title + '\n')

				# write each image as a line of r;g;b values
				for img_path, is_positive in dataset:
					# read img pixels
					img_file = Image.open(img_path)
					pixels = img_file.load()
					img_file.close()
					# write features to file
					features = []
					for x in range(imgs_size[0]):
						for y in range(imgs_size[1]):
							features.extend([str(e) for e in pixels[x, y]])
					knn_features_file.write(';'.join(features) + ';' + str(is_positive) + '\n')
				knn_features_file.close()

			if debug:
				print('done')
		
		# add negative samples
		avg_n_images_in_strategy = int(avg_n_images_in_strategy * 2 / len(strategies))

		# create train set from VOC train dataset
		if train_voc_filename not in dataset_train_voc_class_dir_content:
			if debug:
				print('Preparing KNN features for \'{}/{}\' ... '.format(data_class, 'voc'), end='')
			####
			# get samples
			#
			voc_images_dir = os.path.join('voc', 'images')
			voc_train_class_dir = os.path.join('voc', 'train', data_class)
			# add a number of samples equals to the number of images on this strategy plus its negatives
			dataset = []
			voc_train_class_labels = open(os.path.join(voc_train_class_dir, 'labels.txt'))
			line = voc_train_class_labels.readline()
			for i in range(avg_n_images_in_strategy):
				img_name, is_positive = line.split()
				# negatives are -1 in the file, setting it to 0
				is_positive = max(0, int(is_positive))
				dataset.append((os.path.join(voc_images_dir, img_name + '.jpg'), is_positive))
				line = voc_train_class_labels.readline()
			voc_train_class_labels.close()

			####
			# create features file
			#
			knn_features_path = os.path.join(dataset_train_voc_class_dir, train_voc_filename)
			knn_features_file = open(knn_features_path, 'w')

			# write title in csv
			title = ['r_{n};g_{n};b_{n}'.format(n=e) for e in range(imgs_size[0] * imgs_size[1])]
			title.append('is_positive')
			title = ';'.join(title)
			knn_features_file.write(title + '\n')

			# write each image as a line of r;g;b values
			for img_path, is_positive in dataset:
				# read img pixels
				img_file = Image.open(img_path)
				img_file = img_file.resize(imgs_size, Image.BICUBIC)
				pixels = img_file.load()
				img_file.close()
				# write features to file
				features = []
				for x in range(imgs_size[0]):
					for y in range(imgs_size[1]):
						features.extend([str(e) for e in pixels[x, y]])
				knn_features_file.write(';'.join(features) + ';' + str(is_positive) + '\n')
			knn_features_file.close()

			if debug:
				print('done')

		# create test set from VOC val dataset
		if test_filename not in dataset_test_class_dir_content:
			if debug:
				print('Preparing KNN features for \'{}/{}\' ... '.format(data_class, 'test'), end='')
			####
			# get samples
			#
			test_images_dir = os.path.join('voc', 'images')
			test_class_dir = os.path.join('voc', 'val', data_class)
			# add a number of samples equals to the number of images on this strategy plus its negatives
			dataset = []
			test_class_labels = open(os.path.join(test_class_dir, 'labels.txt'))
			line = test_class_labels.readline()
			for i in range(avg_n_images_in_strategy):
				img_name, is_positive = line.split()
				# negatives are -1 in the file, setting it to 0
				is_positive = max(0, int(is_positive))
				dataset.append((os.path.join(test_images_dir, img_name + '.jpg'), is_positive))
				line = test_class_labels.readline()
			test_class_labels.close()

			####
			# create features file
			#
			knn_features_path = os.path.join(dataset_test_class_dir, test_filename)
			knn_features_file = open(knn_features_path, 'w')

			# write title in csv
			title = ['r_{n};g_{n};b_{n}'.format(n=e) for e in range(imgs_size[0] * imgs_size[1])]
			title.append('is_positive')
			title = ';'.join(title)
			knn_features_file.write(title + '\n')

			# write each image as a line of r;g;b values
			for img_path, is_positive in dataset:
				# read img pixels
				img_file = Image.open(img_path)
				img_file = img_file.resize(imgs_size, Image.BICUBIC)
				pixels = img_file.load()
				img_file.close()
				# write features to file
				features = []
				for x in range(imgs_size[0]):
					for y in range(imgs_size[1]):
						features.extend([str(e) for e in pixels[x, y]])
				knn_features_file.write(';'.join(features) + ';' + str(is_positive) + '\n')
			knn_features_file.close()

			if debug:
				print('done')

	####
	# KNN CLASSIFIER
	#
	dataset_results_dir = assure_dir(datasets_dir, 'results')
	dataset_results_knn_dir = assure_dir(dataset_results_dir, 'knn')
	results_filename = 'results_knn.joblib'
	ap_scores_filename = 'ap_scores_knn.joblib'

	ap_scores_file = os.path.join(dataset_results_knn_dir, ap_scores_filename)

	ap_scores = {}
	if not os.path.exists(ap_scores_file):
		# results are yet to be computed
		labels = ['negative', 'positive']
		for strategy in os.listdir(datasets_train_dir):

			# dir is in the pattern <dataset>/train/<strategy>
			dataset_train_strategy_dir = os.path.join(datasets_train_dir, strategy)
			file_csv = train_voc_filename if strategy == 'voc' else train_prometheus_filename

			dataset_results_knn_strategy_dir = assure_dir(dataset_results_knn_dir, strategy)

			for clazz in os.listdir(dataset_train_strategy_dir):

				dataset_results_knn_strategy_class_dir = assure_dir(dataset_results_knn_strategy_dir, clazz)
				results_file = os.path.join(dataset_results_knn_strategy_class_dir, results_filename)

				# loading test set
				test_csv_path = os.path.join(datasets_test_dir, clazz, test_filename)
				test_dataset = pd.read_csv(test_csv_path, sep=';')
				test_attr = test_dataset.iloc[:, :-1].values
				test_labels = test_dataset.iloc[:, 3072].values

				if os.path.exists(results_file):
					# classifier already trained
					classifier = load(results_file)
				else:
					# loading train set
					train_csv_path = os.path.join(dataset_train_strategy_dir, clazz, file_csv)
					train_set = pd.read_csv(train_csv_path, sep=';')
					train_attr = train_set.iloc[:, :-1].values
					train_labels = train_set.iloc[:, 3072].values

					# rescale features (uniform values from [0,255] to [0,1])
					scaler = StandardScaler()
					scaler.fit(train_attr)
					train_attr = scaler.transform(train_attr)
					test_attr = scaler.transform(test_attr)

					# classify and predict
					k = train_attr.shape[0] // 2 - 1
					classifier = KNeighborsClassifier(n_neighbors=k)
					classifier.fit(train_attr, train_labels)

					# save classifier
					dump(classifier, results_file)

				pred_labels = classifier.predict(test_attr)

				# evaluation
				ap_score = average_precision_score(test_labels, pred_labels)
				if strategy not in ap_scores:
					ap_scores[strategy] = []
				ap_scores[strategy].append((clazz, ap_score))

				if debug:
					print()
					print('##### \'{}/{}\' dataset performance #####'.format(clazz, strategy))
					print(ap_score)
					print(confusion_matrix(test_labels, pred_labels))
					print(classification_report(test_labels, pred_labels, target_names=labels))

		# save results
		dump(ap_scores, ap_scores_file)
	else:
		# results already computed
		ap_scores = load(ap_scores_file)

	# plot results
	filter_strategies = True
	plt.figure(figsize=(12, 6))
	plt.title('KNN results')
	for strategy, datasets in ap_scores.items():
		if filter_strategies:
			if strategy in ['google', 'fcomb', 'voc']:
				datasets.sort(key=lambda e: e[0])
				xlabels = [e[0] for e in datasets]
				ylabels = [e[1] for e in datasets]
				plt.plot(xlabels, ylabels, label=strategy)
		else:
			datasets.sort(key=lambda e: e[0])
			xlabels = [e[0] for e in datasets]
			ylabels = [e[1] for e in datasets]
			plt.plot(xlabels, ylabels, label=strategy)
	plt.xlabel('Pascal VOC 2012 Object classes')
	plt.ylabel('Average Precision (AP)')
	plt.legend(loc='best')
	plt.show()


if __name__ == '__main__':
	main()
