import os

import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from joblib import dump, load
from keras import Input, Model
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Dropout
from keras.models import Sequential
from sklearn.metrics import average_precision_score

import prometheus
from utils import assure_dir

voc_classes = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
               'cow', 'dog', 'horse', 'motorbike', 'person', 'sheep', 'sofa', 'train', 'tv']

voc_classes = voc_classes[:]
n_voc_classes = len(voc_classes)


def main():
	####
	# CNN metrics
	#
	# a 16 layers convnet is prepared inspired to the VGG16 architecture and it is trained each time using
	#   1. fcomb strategy dataset
	#   2. google strategy dataset
	#   3. voc train dataset
	# and it is tested on the voc valid dataset
	#
	
	debug = True
	n_imgs = 100
	imgs_size = (32, 32)
	
	# init a dataset for each class
	data_paths = []
	for clazz in voc_classes:
		keyword_dataset_path = \
			prometheus.steal(clazz, n_bigrams=10, n_imgs=n_imgs, imgs_size=imgs_size, clean=False, quiet=not debug)
		data_paths.append(keyword_dataset_path)
	
	####
	# prepare for CNN
	#
	datasets_dir = assure_dir('datasets')
	datasets_test_dir = assure_dir(datasets_dir, 'test')
	datasets_train_dir = assure_dir(datasets_dir, 'train')
	
	if debug:
		print('Preparing CNN train set ... ')
	
	# build train set for each strategy
	train_prometheus_filename = 'train_prometheus_knn_features.csv'
	train_voc_filename = 'train_voc_knn_features.csv'
	for strategy in os.listdir(datasets_train_dir):
		
		dataset_train_strategy_dir = os.path.join(datasets_train_dir, strategy)
		train_attr_file = os.path.join(dataset_train_strategy_dir, 'test_attr_cnn.joblib')
		train_labels_file = os.path.join(dataset_train_strategy_dir, 'test_labels_cnn.joblib')
		
		if not os.path.exists(train_attr_file) and not os.path.exists(train_labels_file):
			# dir is in the pattern <dataset>/train/<strategy>
			file_csv = train_voc_filename if strategy == 'voc' else train_prometheus_filename
			
			train_attr = np.empty((0, 32, 32, 3), dtype=int)
			train_labels = np.empty((0, n_voc_classes), dtype=int)
			
			for clazz in os.listdir(dataset_train_strategy_dir):
				
				if debug:
					print('\tReading train set from \'{}/{}\'... '.format(clazz, strategy), end='')
				
				# loading train set
				train_csv_path = os.path.join(dataset_train_strategy_dir, clazz, file_csv)
				class_train_set = pd.read_csv(train_csv_path, sep=';')
				# use binary classification to index out only the positive samples
				class_train_mask = class_train_set.iloc[:, 3072].values.astype(bool)
				class_train_attr = class_train_set.iloc[:, :-1].values.astype(int)
				class_train_attr = class_train_attr.reshape((class_train_attr.shape[0], 32, 32, 3))
				class_train_attr = class_train_attr[class_train_mask, :, :, :]
				# create categorical vector
				class_train_labels = keras.utils.to_categorical(np.array(voc_classes.index(clazz)),
				                                                num_classes=n_voc_classes)
				class_train_labels = np.repeat(class_train_labels[np.newaxis, :], class_train_attr.shape[0], axis=0)
				train_attr = np.vstack((train_attr, class_train_attr))
				train_labels = np.vstack((train_labels, class_train_labels))
				
				if debug:
					print('done')
			
			# save set
			dump(train_attr, train_attr_file)
			dump(train_labels, train_labels_file)
			
			if debug:
				print('\tSaved train set for strategy \'{}\''.format(strategy))
	
	# create ccomb
	dataset_train_ccomb_dir = assure_dir(datasets_train_dir, 'ccomb')
	dataset_train_ccomb_attr_path = os.path.join(dataset_train_ccomb_dir, 'test_attr_cnn.joblib')
	dataset_train_ccomb_labels_path = os.path.join(dataset_train_ccomb_dir, 'test_labels_cnn.joblib')
	
	if not os.path.exists(dataset_train_ccomb_attr_path) and not os.path.exists(dataset_train_ccomb_labels_path):
		train_attr = np.empty((0, 32, 32, 3), dtype=int)
		train_labels = np.empty((0, n_voc_classes), dtype=int)
		for strategy in ['hyp', 'vadj', 'prepar']:
			dataset_train_strategy_dir = os.path.join(datasets_train_dir, strategy)
			train_stategy_attr_path = os.path.join(dataset_train_strategy_dir, 'test_attr_cnn.joblib')
			train_strategy_attr = load(train_stategy_attr_path)
			train_attr = np.vstack((train_attr, train_strategy_attr))
			train_labels_path = os.path.join(dataset_train_strategy_dir, 'test_labels_cnn.joblib')
			train_strategy_labels = load(train_labels_path)
			train_labels = np.vstack((train_labels, train_strategy_labels))
		dump(train_attr, dataset_train_ccomb_attr_path)
		dump(train_labels, dataset_train_ccomb_labels_path)
	
	if debug:
		print('done')
	
	if debug:
		print('Preparing CNN test set ... ', end='')
	
	# build test set
	test_filename = 'test_knn_features.csv'
	test_attr_file = os.path.join(datasets_test_dir, 'test_attr_cnn.joblib')
	test_labels_file = os.path.join(datasets_test_dir, 'test_labels_cnn.joblib')
	if not os.path.exists(test_attr_file) and not os.path.exists(test_labels_file):
		
		test_attr = np.empty((0, 32, 32, 3), dtype=int)
		test_labels = np.empty((0, n_voc_classes), dtype=int)
		
		for clazz in os.listdir(datasets_test_dir):
			# loading test set
			test_csv_path = os.path.join(datasets_test_dir, clazz, test_filename)
			test_dataset = pd.read_csv(test_csv_path, sep=';')
			# use binary classification to index out only the positive samples
			class_test_mask = test_dataset.iloc[:, 3072].values
			class_test_attr = test_dataset.iloc[:, :-1].values.astype(int)
			class_test_attr = class_test_attr.reshape((class_test_attr.shape[0], 32, 32, 3))
			class_test_attr = class_test_attr[class_test_mask, :, :, :]
			# create categorical vector
			class_test_labels = keras.utils.to_categorical(np.array(voc_classes.index(clazz)),
			                                               num_classes=n_voc_classes)
			class_test_labels = np.repeat(class_test_labels[np.newaxis, :], class_test_attr.shape[0], axis=0)
			test_attr = np.vstack((test_attr, class_test_attr))
			test_labels = np.vstack((test_labels, class_test_labels))
		
		# save set
		dump(test_attr, test_attr_file)
		dump(test_labels, test_labels_file)
	
	if debug:
		print('done')
	
	####
	# CNN CLASSIFIER
	#
	dataset_results_dir = assure_dir(datasets_dir, 'results')
	dataset_results_cnn_dir = assure_dir(dataset_results_dir, 'cnn')
	
	imgs_size = (imgs_size[0], imgs_size[1], 3)
	
	# load test set
	x_test = load(test_attr_file)
	y_test = load(test_labels_file)
	
	ap_scores = {}
	for strategy in ['ccomb', 'google', 'voc']:
		
		datasets_train_strategy_dir = os.path.join(datasets_train_dir, strategy)
		train_attr_file = os.path.join(datasets_train_strategy_dir, 'test_attr_cnn.joblib')
		train_labels_file = os.path.join(datasets_train_strategy_dir, 'test_labels_cnn.joblib')
		
		dataset_results_cnn_strategy_dir = assure_dir(dataset_results_cnn_dir, strategy)
		model_file = os.path.join(dataset_results_cnn_strategy_dir, 'model.h5')
		
		x_train = load(train_attr_file)
		y_train = load(train_labels_file)
		
		if not os.path.exists(model_file):
			
			# build and train the evaluation model
			print('Training the network for {} ... '.format(strategy))
			# callback = [
			# 	EarlyStopping(patience=50, verbose=1),
			# 	ModelCheckpoint(model_file, verbose=1, save_best_only=True)
			# ]
			model = build_cnn_architecture_shallow(imgs_size)
			model.fit(x_train, y_train, epochs=20, batch_size=32, shuffle=True)  # , callbacks=callback)
			print('done')
			
			# save the model for later use
			model.save(model_file)
			print('Model of the network saved')
		
		else:
			model = keras.models.load_model(model_file)
		
		# # print how well the network performs
		# print('LOSS and metrics')
		# loss_and_metrics = model.evaluate(x_test, y_test, batch_size=128)
		# print(loss_and_metrics)
		
		# print a prediction for a sample in the test set and its ground truth
		print('Prediction')
		y_pred = model.predict(x_test, batch_size=128)
		ap_score = average_precision_score(y_test, y_pred)
		print(ap_score)
		
		if strategy not in ap_scores:
			ap_scores[strategy] = []
		ap_scores[strategy].append((strategy, ap_score))
	
	# plot results
	plt.figure(figsize=(12, 6))
	plt.title('CNN results')
	for strategy, datasets in ap_scores.items():
		datasets.sort(key=lambda e: e[0])
		xlabels = [e[0] for e in datasets]
		ylabels = [e[1]*100 for e in datasets]
		plt.plot(xlabels, ylabels, label=strategy, marker='o', markersize=5)
	plt.xlabel('Strategy')
	plt.ylabel('Average Precision (AP)')
	plt.legend(loc='best')
	plt.show()
	
	
def build_cnn_architecture_deep(input_shape):
	model = Sequential()
	model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True,
	                 input_shape=input_shape))
	model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(MaxPooling2D(pool_size=(2, 1)))
	model.add(Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(MaxPooling2D(pool_size=(2, 1)))
	model.add(Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu', use_bias=True))
	model.add(MaxPooling2D(pool_size=(2, 1)))
	model.add(Flatten())
	model.add(Dense(units=4096, activation='relu'))
	model.add(Dense(units=4096, activation='relu'))
	model.add(Dense(units=4096, activation='relu'))
	model.add(Dense(units=18, activation='softmax'))
	
	model.compile(loss=keras.losses.mean_squared_logarithmic_error,
	              optimizer=keras.optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True),
	              metrics=['accuracy'])
	
	return model


def build_cnn_architecture_shallow(input_shape):
	inputs = Input(shape=input_shape)
	x = Conv2D(filters=32, kernel_size=(5, 5), padding='same', activation='relu', use_bias=True)(inputs)
	x = MaxPooling2D(pool_size=(2, 2))(x)
	x = Conv2D(filters=32, kernel_size=(5, 5), padding='same', activation='relu', use_bias=True)(x)
	x = MaxPooling2D(pool_size=(2, 2))(x)
	x = Flatten()(x)
	x = Dense(units=1024, activation='relu')(x)
	x = Dropout(0.5)(x)
	predictions = Dense(units=18, activation='softmax')(x)
	model = Model(inputs=inputs, outputs=predictions)
	
	model.compile(loss=keras.losses.mean_squared_logarithmic_error,
	              optimizer=keras.optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True),
	              metrics=['accuracy'])
	
	return model


if __name__ == '__main__':
	main()
