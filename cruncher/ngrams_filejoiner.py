from utils import *


def main():

	ngrams_a_folder = 'ngrams-a'
	ngrams_o_folder = 'ngrams-o'

	for f in os.listdir(ngrams_a_folder):
		f_a = os.path.join(ngrams_a_folder, f)
		f_o = os.path.join(ngrams_o_folder, f)
		if os.path.exists(f_o):

			perr('Merging files {} ... ', f)

			f_a_file = open(f_a, 'a')
			f_o_file = open(f_o, 'r')

			line = f_o_file.readline()
			while line != '':
				f_a_file.write(line)
				f_a_file.flush()
				line = f_o_file.readline()

			perrln('done')

			f_a_file.close()
			f_o_file.close()
			os.remove(f_o)

	for f in os.listdir(ngrams_o_folder):
		f_a = os.path.join(ngrams_a_folder, f)
		f_o = os.path.join(ngrams_o_folder, f)
		os.rename(f_o, f_a)

	os.remove(ngrams_o_folder)
	os.rename(ngrams_a_folder, 'ngrams')


if __name__ == '__main__':
	main()