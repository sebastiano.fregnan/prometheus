from utils import *
import gzip
import shutil

postags = ['NOUN', 'ADJ', 'VERB', 'X', '_']
ngrams_folder = '../ngrams'


def main(off=None):
	
	files = sorted(os.listdir(ngrams_folder))
	off = files.index('ngrams-{}.txt'.format(off)) if off else 0
	
	for f_i in range(off, len(files)):
		file_txt_name = files[f_i]
		file_txt_path = os.path.join(ngrams_folder, file_txt_name)
		file_gz_name = files[f_i] + '.gz'
		file_gz_path = os.path.join(ngrams_folder, file_gz_name)
		with open(file_txt_path, 'rb') as f_in:
			with gzip.open(file_gz_path, 'wb') as f_out:
				shutil.copyfileobj(f_in, f_out)
		os.remove(file_txt_path)
		perrln('Compressed {}'.format(file_txt_name))


if __name__ == '__main__':
	main()
