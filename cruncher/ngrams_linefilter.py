from utils import *
import re

postags = ['NOUN', 'ADJ', 'VERB', 'X', '_']


def main(off=None):
	ngrams_folder = '../ngrams'
	files = sorted(os.listdir(ngrams_folder))
	off = files.index('ngrams-{}.txt'.format(off)) if off else 0
	for f_i in range(off, len(files)):
		f = files[f_i]
		f_path = os.path.join(ngrams_folder, f)
		if ' ' in f:
			os.remove(f_path)
			continue
		f_file = open(f_path, 'r')
		lines = sorted(f_file.readlines())
		f_file.close()
		line = [None] * 5
		for i in range(len(lines)):
			prev = line
			line = re.sub(' +', ' ', lines[i])
			line = line.split(' ')
			if len(line) != 5:
				line = [e for e in line if e != '']
				if line[0] in postags or line[0] == '' or line[0][-1] == '\n':
					line.insert(0, '_')
				if line[1] not in postags:
					line.insert(1, '_')
				if line[2] in postags or line[2] == '' or line[2][-1] == '\n':
					line.insert(2, '_')
				if line[3] not in postags:
					line.insert(3, '_')
				if line[4][-1] != '\n':
					line.insert(4, '0\n')
				lines[i] = ' '.join(line)
			
			if line[:3] == prev[:3] and line[4] == prev[4]:
				lines[i] = ''
	
			if line[:4] == prev[:4]:
				lines[i-1] = ''
				line[4] = str(int(line[4][:-1]) + int(prev[4][:-1])) + '\n'
				lines[i] = ' '.join(line)

		f_file = open(f_path, 'w')
		f_file.writelines(lines)
		f_file.close()

		perrln('Filtered {}'.format(f))


if __name__ == '__main__':
	main('reu')
