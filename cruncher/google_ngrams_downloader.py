import gzip
import urllib.request as request
from utils import *

alpha = 'abcdefghijklmnopqrstuvwxyz'
tags = ['_ADJ', '_ADP', '_ADV', '_CONJ', '_DET', '_NOUN', '_NUM', '_PRON', '_PRT', '_VERB']


def download_datasets(first_letter=None, second_letter=None):
	# check folders and files
	google_ngrams_folder = 'google_ngrams'
	if not os.path.exists(google_ngrams_folder):
		os.mkdir(google_ngrams_folder)
	log_path = os.path.join(google_ngrams_folder, 'progress.log')
	if not os.path.exists(log_path):
		open(log_path, 'a').close()

	# ignore log if offset specified
	if not first_letter:
		log = open(log_path, 'r')

		# read last line in log file
		log_line = None
		for log_line in log:
			pass
		log.close()

		# skip already done files
		if log_line:
			(log_file_name, log_file_status) = log_line.split('@', 1)
			log_file_status = Log.Progress(log_file_status[:-1])
			a_offset = alpha.index(log_file_name[0])
			b_offset = alpha.index(log_file_name[1])
			if log_file_status is Log.Progress.DONE:
				b_offset += 1
		else:
			log_file_status = Log.Progress.DONE
			a_offset = 0
			b_offset = 0
	else:
		log_file_status = Log.Progress.DONE
		a_offset = alpha.index(first_letter)
		b_offset = alpha.index(second_letter) if second_letter else 0

	log = open(log_path, 'a')

	def _log(status: Log.Progress):
		nonlocal log_file_name, log_file_status
		log_file_status = status
		log.write('{}@{}\n'.format(log_file_name, log_file_status.value))
		log.flush()

	for a in alpha[a_offset:]:
		for b in alpha[b_offset:]:

			log_file_name = a + b

			if log_file_status is Log.Progress.DONE:
				# prev file completed, begin new file
				_log(Log.Progress.BEGIN)

			file_gz_name = 'googlebooks-eng-all-2gram-20120701-{}.gz'.format(log_file_name)
			file_gz_path = os.path.join(google_ngrams_folder, file_gz_name)

			file_txt_name = 'googlebooks-eng-all-2gram-20120701-{}.txt'.format(log_file_name)
			file_txt_path = os.path.join(google_ngrams_folder, file_txt_name)

			if log_file_status is Log.Progress.BEGIN:
				perrln('Begin processing {}', file_gz_name)
				# download or use cached
				if not os.path.exists(file_gz_path):
					_log(Log.Progress.DOWNLOADING)
					request.urlretrieve('http://storage.googleapis.com/books/ngrams/books/' + file_gz_name, file_gz_path)
					_log(Log.Progress.DOWNLOADED)
			
					perrln('done')
				else:
					_log(Log.Progress.CACHED)
					perrln('using cached file')

			if log_file_status is Log.Progress.DOWNLOADING:
				# restart download because incomplete
				perrln('Interrupted, downloading again {}', file_gz_name)
				request.urlretrieve('http://storage.googleapis.com/books/ngrams/books/' + file_gz_name, file_gz_path)
				_log(Log.Progress.DOWNLOADED)
				perrln('done')

			if log_file_status is Log.Progress.DECOMPRESSING:
				# interrupted in middle of decompression, restart
				_log(Log.Progress.CACHED)
				perrln('Interrupted, decompressing again {}', file_gz_name)

			if log_file_status is Log.Progress.DOWNLOADED \
					or log_file_status is Log.Progress.CACHED:
				# file retrieved, begin decompression
				_log(Log.Progress.DECOMPRESSING)
				perr(' => decompressing ... ')

				with gzip.open(file_gz_path, 'rb') as file_gz:

					line = []
					line_idx = -1

					def _readline():
						nonlocal file_gz, line, line_idx
						line = file_gz.readline().decode('utf-8').split('\t')
						line_idx += 1

					# first line of file sets the baseline
					_readline()
					(ngram, match_count) = (line[0], int(line[2]))
					# begin summarization
					with open(file_txt_path, 'w', encoding='utf-8') as file_txt:
						_readline()
						while len(line) != 1:
							#
							# ngram TAB year TAB match_count TAB volume_count NEWLINE
							#
							if ngram != line[0]:
								# found a new ngram, write in summary the prev ngram
								file_txt.write('{}\t{}\n'.format(ngram, match_count))
								file_txt.flush()
								# set new baseline with current ngram
								(ngram, match_count) = (line[0], int(line[2]))
							else:
								# same bigram, increment frequencies
								match_count += int(line[2])
							_readline()
						file_txt.flush()
				_log(Log.Progress.DECOMPRESSED)
				perrln('done')

			if log_file_status is Log.Progress.DECOMPRESSED:
				# empty .gz cache
				_log(Log.Progress.DELETING)
				perr(' => deleting ... ')
				os.remove(file_gz_path)
				_log(Log.Progress.DELETED)
				perrln('done')

			if log_file_status is Log.Progress.DELETING:
				# maybe file still exists
				if os.path.exists(file_gz_path):
					perrln('Interrupted, deleting again {}', file_gz_name)
					perr(' => deleting ... ')
					os.remove(file_gz_path)
					_log(Log.Progress.DELETED)
					perrln('done')
				else:
					_log(Log.Progress.DELETED)
					perrln('Interrupted, but already deleted {}', file_gz_name)

			if log_file_status is Log.Progress.DELETED:
				# everything's done
				_log(Log.Progress.DONE)
				perrln(' ## completed processing {}', file_gz_name)
				perrln()

		b_offset = 0
	log.close()


if __name__ == '__main__':
	l1 = sys.argv[1] if len(sys.argv) > 1 else None
	l2 = sys.argv[2] if len(sys.argv) > 2 else None
	download_datasets(l1, l2)
