import string
import unidecode
from utils import *

alpha = 'abcdefghijklmnopqrstuvwxyz'
postags = ["NOUN", "VERB", "ADJ", "ADV", "PRON", "DET", "ADP", "NUM", "CONJ", "PRT", "END", ".", "X"]
postags_ = ["_NOUN_", "_VERB_", "_ADJ_", "_ADV_", "_PRON_", "_DET_", "_ADP_", "_NUM_", "_CONJ_", "_PRT_", "_END_" "_._", "_X_"]
postags__ = ['NOUN', 'ADJ', 'VERB', 'X', '_']


def main(first_letter=None, second_letter=None):

	ngrams_folder = 'ngrams'
	if not os.path.exists(ngrams_folder):
		os.mkdir(ngrams_folder)

	# init log
	log_name = '_progress.log'
	log_path = os.path.join(ngrams_folder, log_name)

	# ignore log if offset specified
	if not first_letter:
		log = Log(log_path, resume=True)
		# skip already done files
		if log.is_empty():
			a_offset = 0
			b_offset = 0
		else:
			a_offset = alpha.index(log.file_code[0])
			b_offset = alpha.index(log.file_code[1])
			if log.file_status is Log.Progress.DONE:
				b_offset += 1
	else:
		log = Log(log_path, resume=False)
		a_offset = alpha.index(first_letter)
		b_offset = alpha.index(second_letter) if second_letter else 0

	file_txt, line, line_idx, done_size = None, '', 0, 0
	lemma1, tag1, lemma2, tag2, freq = '', '', '', '', 0
	punct_filter = dict((ord(c), '_') for c in string.punctuation)
	
	def _readline():
		nonlocal file_txt, line, line_idx, done_size, lemma1, tag1, lemma2, tag2, freq
		line = file_txt.readline()
		done_size += len(line)
		# reached EOF
		if line is None or line == '':
			return False
		lemmas, freq = line[: -1].split('\t')
		line_idx += 1
		# normalize content of line
		lemma1, lemma2 = lemmas.split(' ')
		lemma1, *tag1 = lemma1.rsplit('_', 1)
		lemma1 = unidecode.unidecode(lemma1).lower()
		tag1 = tag1[0] if tag1 and tag1[0] in postags else '_'
		# filter out bigrams with postag-only lemmas
		if lemma2 in postags_:
			lemma2 = '_'
			tag2 = '.'  # this is the actual filter
			return True
		lemma2, *tag2 = lemma2.rsplit('_', 1)
		lemma2 = unidecode.unidecode(lemma2).lower()
		tag2 = tag2[0] if tag2 and tag2[0] in postags else '_'
		freq = int(freq)
		return True

	google_ngrams_folder = 'google_ngrams'
	
	ttl = 8
	ttl_files_rev = {}
	opened_files_rev = {}

	for a in alpha[a_offset:]:
		for b in alpha[b_offset:]:
			log_file_name = a + b
			file_txt_name = 'googlebooks-eng-all-2gram-20120701-{}.txt'.format(log_file_name)
			file_txt_path = os.path.join(google_ngrams_folder, file_txt_name)

			line_idx, done_size = 0, 0

			if log.file_status is Log.Progress.DONE:
				log.file(log_file_name)
				log(Log.Progress.BEGIN)

			if log.file_status is Log.Progress.BEGIN:
				perrln('Begin processing file {}', file_txt_name)
				log(Log.Progress.READY)

			if log.file_status is Log.Progress.DIVIDING:
				perrln('Interrupted, no checkpoint saved for {}', file_txt_name)
				log(Log.Progress.READY)

			if log.file_status is Log.Progress.READY \
				or log.file_status is Log.Progress.LINE:
				with open(file_txt_path, 'r', encoding='utf-8') as file_txt:

					if log.file_status is Log.Progress.LINE and log.has_data():
						perr('Interrupted, restoring previous checkpoint for {} ...', file_txt_name)
						for i in range(int(log.file_data[0]) - 1):
							# redo last line
							file_txt.readline()
						perrln('done')
						log(Log.Progress.READY)

					if log.file_status is Log.Progress.READY:
						perr(' => dividing bigrams in sub files ... ')
						log(Log.Progress.DIVIDING)
						try:
							ttl_files = {}
							opened_files = {}
							perc_labels = list(range(5, 101, 5))
							file_size = os.path.getsize(file_txt_path)
							
							while _readline():
								lem1 = lemma1[:3].translate(punct_filter)
								lem2 = lemma2[:3].translate(punct_filter)
								if tag2 in postags__ and not lem2.startswith('_') and lem2 != '' and not lem2[0].isnumeric() \
									and tag1 in postags__ and not lem1.startswith('_'):
									if lem1 not in opened_files:
										opened_files[lem1] = open(
											os.path.join(ngrams_folder, 'ngrams-{}.txt').format(lem1),
											mode='a', encoding='utf-8')
										ttl_files[lem1] = ttl
									opened_files[lem1].write('{} {} {} {} {}\n'.format(lemma1, tag1, lemma2, tag2, freq))
									opened_files[lem1].flush()
									ttl_files[lem1] += 1

									if lem2 not in opened_files_rev:
										opened_files_rev[lem2] = open(
											os.path.join(ngrams_folder, 'ngrams-rev-{}.txt').format(lem2),
											mode='a', encoding='utf-8')
										ttl_files_rev[lem2] = ttl
									opened_files_rev[lem2].write('{} {} {} {} {}\n'.format(lemma2, tag2, lemma1, tag1, freq))
									opened_files_rev[lem2].flush()
									ttl_files_rev[lem2] += 1

								# show progress
								perc = int(100*done_size/file_size)
								if perc in perc_labels:
									perr('#')
									perc_labels.pop(0)
								
								# update ttl of opened files
								files_to_remove = []
								for l, t in ttl_files.items():
									if t > 0:
										ttl_files[l] = t - 1
									else:
										files_to_remove.append(l)
								for l in files_to_remove:
									del ttl_files[l]
									opened_files[l].close()
									del opened_files[l]

								files_to_remove = []
								for l, t in ttl_files_rev.items():
									if t > 0:
										ttl_files_rev[l] = t - 1
									else:
										files_to_remove.append(l)
								for l in files_to_remove:
									del ttl_files_rev[l]
									opened_files_rev[l].close()
									del opened_files_rev[l]
							
							# close currently opened files
							files_to_remove = []
							for l, t in ttl_files.items():
								files_to_remove.append(l)
							for l in files_to_remove:
								del ttl_files[l]
								opened_files[l].close()
								del opened_files[l]
							
							log(Log.Progress.DONE)
							perrln(' done')
						except BaseException as ex:
							perrln(' error {}', ex)
							log(Log.Progress.LINE, line_idx)
							sys.exit(1)
		b_offset = 0
	# close all common reversed bigrams files
	files_to_remove = []
	for l, t in ttl_files_rev.items():
		files_to_remove.append(l)
	for l in files_to_remove:
		del ttl_files_rev[l]
		opened_files_rev[l].close()
		del opened_files_rev[l]


if __name__ == '__main__':
	l1 = sys.argv[1] if len(sys.argv) > 1 else None
	l2 = sys.argv[2] if len(sys.argv) > 2 else None
	main(l1, l2)
