from utils import *
import subprocess

ngrams_folder = '../ngrams'


def main():
	for f in os.listdir(ngrams_folder):
		f_path = os.path.join(ngrams_folder, f)
		subprocess.call(['sort', f_path, '-o', f_path])
		perrln('Sorted {}'.format(f_path))


if __name__ == '__main__':
	main()
