import os
import shutil
from utils import assure_dir

voc_classes = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
               'cow', 'dog', 'horse', 'motorbike', 'person', 'sheep', 'sofa', 'train', 'tv']


def main():

	assure_dir(os.path.join('..', 'datasets'))
	assure_dir(os.path.join('..', 'datasets', 'voc'))

	sets = [(os.path.join('..', 'VOC2012'), assure_dir('..', 'datasets', 'voc', 'train'), '_train.txt'),
	        (os.path.join('..', 'VOC2012'), assure_dir('..', 'datasets', 'voc', 'val'), '_val.txt')]

	for src, dst, suffix in sets:
		labels_dir = os.path.join(src, 'labels')
		for clazz in voc_classes:

			assure_dir(dst, clazz)
			src_labels = os.path.join(labels_dir, clazz + suffix)
			dst_labels = os.path.join(dst, clazz, 'labels.txt')
			shutil.copy2(src_labels, dst_labels)

	shutil.copytree(os.path.join('..', 'VOC2012', 'images'), os.path.join('..', 'datasets', 'voc', 'images'))


if __name__ == '__main__':
	main()
